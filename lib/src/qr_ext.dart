import 'dart:typed_data';
import 'package:flutter/services.dart';

class QrUtilExt {
  static const MethodChannel _channel =
      const MethodChannel('com.aeologic.adhoc.qr_utils');

  static Future<Uint8List> generateQR(String content) async {
    final Uint8List uInt8list =
        await _channel.invokeMethod('generateQR', {"content": content});
    return uInt8list;
  }
}
